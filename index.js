/*
 Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries", 
    "Gunther Smith", 
    "Macie West", 
    "Michelle Queen", 
    "Shane Miguelito", 
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
        function register(registerName){
            if (registeredUsers.includes(registerName) === false){
                registeredUsers.push(registerName);
                alert("Thank you for registering!");
                console.log(registeredUsers.toString());
            }
            else {
                alert("Registration Failed! Username already exists!");
            }
        }        

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
        function addFriend(newFriend){
            if(registeredUsers.includes(newFriend) === false){
                alert("User not found.");
            }

            else if(friendsList.includes(newFriend) == true){
                    alert("You've already added this friend!");
            }

            else{
                friendsList.push(newFriend);
                alert("You have added " +newFriend+ " as a friend!");
                //console.log(friendsList.toString());
            }
        }

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
        function displayFriends(){
            if(friendsList.length == 0){
                alert("You currently have 0 friends. Add one first");
            }
            else{
                friendsList.forEach(function(friend){
                    console.log(friend);
                })
                //console.log(friendsList.join("\n"));
                // for(let i = 0; i < friendsList.length; i++){
                //     console.log(friendsList[i]);
                }   
            //}
        }
/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
        function displayNumberOfFriends(){
            if(friendsList.length == 0){
                alert("You currently have 0 friends. Add one first");
            }
            else{
                alert("You have " +friendsList.length+ " friends.");
            }
        }
/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/ 
        function deleteFriend(unfriend){
            
            if(friendsList.length == 0){
                alert("You currently have 0 friends. Add one first");
            }

            else if(friendsList.includes(unfriend) === false){
                alert(unfriend+ " is not on your friend's list.");
            }

            else{

                /*for (let index = 0; index < friendsList.length; index++){
                    if(friendsList[index] === unfriend){*/
                    let indexOfUnfriend = friendsList.indexOf(unfriend);
                        let spliced = friendsList.splice(indexOfUnfriend, 1);
                        alert(unfriend + " has been removed from the list.");
                        console.log(friendsList.toString());
            /*        }
                }*/
            }
        }

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().
*/